export default () => {
    const pageNavToggle = document.querySelectorAll('.nav-toggle');

    if (pageNavToggle) {
        pageNavToggle.forEach(el => {
            el.addEventListener('click', (e) => {
                const self = document.querySelector('.root');
                self.classList.toggle('nav-open');
                $('.mobile-nav__second').removeClass('mobile-nav__second--open');
                return false;
            });
        });
    }

    $('.mobile-nav__next').on('click', function(e){
        e.preventDefault();
        let secondNav = '.' + $(this).attr('data-nav');
        $('.mobile-nav__subnav').removeClass('active');
        $(secondNav).addClass('active');
        $('.mobile-nav__second').addClass('mobile-nav__second--open');
    });

    $('.mobile-nav__back').on('click', function(e){
        e.preventDefault();
        $('.mobile-nav__second').removeClass('mobile-nav__second--open');
    });


};

